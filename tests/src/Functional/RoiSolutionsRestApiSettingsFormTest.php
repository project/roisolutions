<?php

namespace Drupal\Tests\roisolutions\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Tests the REST API settings form.
 *
 * @group roisolutions
 */
class RoiSolutionsRestApiSettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['roisolutions', 'key'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected $profile = 'minimal';

  /**
   * User with the permissions to edit module settings.
   */
  protected UserInterface $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser(['administer roisolutions']);
  }

  /**
   * Tests access to the REST API settings form.
   */
  public function testAccessSettingsForm(): void {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet(Url::fromRoute('roisolutions.settings.rest_api'));
    $this->assertSession()->statusCodeEquals(200);
  }

}
