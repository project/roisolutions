CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Usage
* Support requests
* Maintainers


INTRODUCTION
------------

ROI Solutions provides a service for interaction with the [ROI Solutions](https://roisolutions.com/)
APIS. Note the [REST API](https://secure2.roisolutions.net/api/help/) is the
only currently supported API service.


REQUIREMENTS
------------

This module requires the following modules:

* [Key](https://www.drupal.org/project/key) - For storing API username and
  password.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See
[Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules)
for details.


CONFIGURATION
-------------

Configure API authentication settings by navigating to Configuration > ROI
Solutions > REST API (`/admin/config/roisolutions/settings/rest-api`).


USAGE
------------

The ROI Solutions module provides a single service -- the REST API client.

```php
/** @var \Drupal\roisolutions\RestApi $client */
$restApi = Drupal::service('roisolutions.rest_api');
$donor = $restApi->getClient()->getDonor("123456");
```


SUPPORT REQUESTS
----------------

* Before posting a support request, carefully read the installation
  instructions provided in module documentation page.

* Before posting a support request, check the Recent Log entries at
  `/admin/reports/dblog`.

* Once you have done this, you can post a support request at module issue
  queue: [https://www.drupal.org/project/issues/roisolutions](https://www.drupal.org/project/issues/roisolutions)

* When posting a support request, please inform if you were able to see any
  errors in the Recent Log entries.


MAINTAINERS
-----------

Current maintainers:

* [Christopher C. Wells (wells)](https://www.drupal.org/u/wells)

Development sponsored by:

* [Cascade Public Media](https://www.drupal.org/cascade-public-media)
