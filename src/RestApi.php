<?php

namespace Drupal\roisolutions;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\State\StateInterface;
use Drupal\key\KeyRepositoryInterface;
use OpenPublicMedia\RoiSolutions\Rest\Client;

/**
 * ROI Solutions REST API client.
 *
 * @package Drupal\roisolutions
 */
final class RestApi {

  /**
   * ROI Solutions config.
   */
  private ImmutableConfig $config;

  /**
   * ROI Solutions REST API client.
   */
  private Client $client;

  /**
   * Origination vendor.
   */
  private readonly ?string $originationVendor;

  /**
   * Constructs the API client wrapper.
   */
  public function __construct(
    private readonly ConfigFactoryInterface $configFactory,
    private readonly KeyRepositoryInterface $keyRepository,
    protected readonly LoggerChannelInterface $logger,
    private readonly StateInterface $state
  ) {
    $this->config = $this->configFactory->get('roisolutions.rest_api');
    $credentials = $this->keyRepository->getKey($this->config->get('user_password'))->getKeyValues();
    if (!isset($credentials['username']) || !isset($credentials['password'])) {
      $this->logger->critical('Provided authentication key missing `username` or `password` property.');
      return;
    }
    $client_code = $this->config->get('client_code');
    if (empty($client_code)) {
      $this->logger->critical('Client code not configured.');
      return;
    }
    $parameters = [
      'userId' => $credentials['username'],
      'password' => $credentials['password'],
      'clientCode' => $client_code,
      'cache' => $this->state,
    ];
    if ($endpoint = $this->config->get('endpoint')) {
      $parameters['baseUri'] = $endpoint;
    }
    $this->client = new Client(...$parameters);
    $this->originationVendor = $this->config->get('origination_vendor');
  }

  /**
   * Gets the ROI Solutions REST API client.
   */
  public function getClient(): Client {
    return $this->client;
  }

  /**
   * Gets the configured origination vendor.
   */
  public function getOriginationVendor(): ?string {
    return $this->originationVendor;
  }

}
