<?php

namespace Drupal\roisolutions\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a general settings form for ROI Solutions REST API.
 *
 * @package Drupal\roisolutions\Form
 */
final class RestApiSettingsForm extends ConfigFormBase {

  /**
   * Gets the name of the config the form modifies.
   */
  private static function getConfigName(): string {
    return 'roisolutions.rest_api';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [self::getConfigName()];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'roisolutions_rest_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(self::getConfigName());
    $form['user_password'] = [
      '#title' => 'User and password key',
      '#type' => 'key_select',
      '#empty_option' => $this->t('- Select -'),
      '#key_filters' => ['type' => 'user_password'],
      '#default_value' => $config->get('user_password'),
      '#required' => TRUE,
      '#disabled' => $this->configIsOverridden('user_password'),
    ];
    $form['client_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client code'),
      '#default_value' => $config->get('client_code'),
      '#required' => TRUE,
      '#disabled' => $this->configIsOverridden('client_code'),
    ];
    $form['endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint'),
      '#description' => $this->t('API endpoint to use for queries. If not set the default endpoint will be used.'),
      '#default_value' => $config->get('endpoint'),
      '#disabled' => $this->configIsOverridden('endpoint'),
    ];
    $form['origination_vendor'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Origination vendor'),
      '#description' => $this->t('Origination vendor to be associated resource creation queries (can be overridden for individual queries).'),
      '#default_value' => $config->get('origination_vendor'),
      '#disabled' => $this->configIsOverridden('origination_vendor'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config($this->getConfigName());
    foreach (['user_password', 'client_code', 'endpoint', 'origination_vendor'] as $key) {
      if (!$this->configIsOverridden($key)) {
        $config->set($key, $form_state->getValue($key));
      }
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Check if a config variable is overridden by local settings.
   */
  protected function configIsOverridden(string $setting_name): bool {
    return $this->configFactory->getEditable(self::getConfigName())->get($setting_name)
      != $this->configFactory->get(self::getConfigName())->get($setting_name);
  }

}
